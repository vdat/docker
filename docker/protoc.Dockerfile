FROM znly/protoc

RUN apk update && apk add ca-certificates && update-ca-certificates && apk add --update openssl curl nodejs nodejs-npm
RUN npm install -g grpc grpc-tools ts-protoc-gen
