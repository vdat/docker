# https://www.npmjs.com/package/grpc-web

FROM ubuntu:18.04

ENV GO_VERSION=1.11
ENV PROTOC_VERSION=3.7.1
ENV GRPC_VERSION=v1.17.0
ENV GRPC_WEB_VERSION=1.0.4
ENV GOPATH=/root/go
ENV PATH="$PATH:/usr/local/go/bin:$GOPATH/bin"

WORKDIR /root

# Install Go
RUN apt-get update && apt-get install -y sudo wget curl git unzip make g++ autoconf libtool && \
    wget https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go${GO_VERSION}.linux-amd64.tar.gz && \
    go get -u google.golang.org/grpc && \
    go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger

# Install Protobuf
RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/protoc-${PROTOC_VERSION}-linux-x86_64.zip && \
    unzip protoc-${PROTOC_VERSION}-linux-x86_64.zip && \
    mv include/google/ /usr/local/include/ && mv bin/protoc /usr/local/bin/protoc && \
    chmod +x /usr/local/bin/protoc

# Clone gRPC
#RUN git clone https://github.com/grpc/grpc.git && \
#    cd grpc && git checkout tags/${GRPC_VERSION} && cd - && \
#    ln -s /root/grpc/src/proto/grpc/ /usr/local/include/

# Install gRPC Web
RUN wget https://github.com/grpc/grpc-web/releases/download/${GRPC_WEB_VERSION}/protoc-gen-grpc-web-${GRPC_WEB_VERSION}-linux-x86_64 && \
    mv protoc-gen-grpc-web-${GRPC_WEB_VERSION}-linux-x86_64 /usr/local/bin/protoc-gen-grpc-web && \
    chmod +x /usr/local/bin/protoc-gen-grpc-web && \ 
    rm -rf /var/lib/apt/lists/*

